const express = require("express");
var amqp = require("amqplib/callback_api");

const app = express();

app.use(express.static(__dirname + "/"));
app.set("view engine", "html");

app.get("/", (req, res) => {
  res.render("./index.html");
});

app.post("/", async (req, res) => {
  const result = await sendFileToAMQP();
  res.status(200).send(result);
});

app.listen("3001", "0.0.0.0");

amqp.connect("amqp://guest:guest@$mq-service:5672", function(error0, connection) {
  if (error0) {
    throw error0;
  }
  connection.createChannel(function(error1, channel) {});
});

function sendFileToAMQP() {
  return Promise.resolve(true);
}
