import { createParamDecorator } from "@nestjs/common";

export const User = createParamDecorator(
  (data, [,,ctx,]) => ctx.req.user,
);