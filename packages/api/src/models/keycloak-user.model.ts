export interface IKeycloakUser {
  keycloakId: string;
  name: string;
  email: string;
  roles: string[];
}
