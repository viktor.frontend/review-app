import { IKeycloakUser } from "./keycloak-user.model";

export interface IKeycloakTokenData extends IKeycloakUser {
  jti: string;
  exp: number;
  nbf: number;
  iat: number;
  iss: string;
  aud: string[];
  sub: string;
  typ: string;
  azp: string;
  auth_time: number;
  session_state: string;
  acr: string;
  'allowed-origins': string[];
  realm_access: {
    roles: string[];
  };
  resource_access: {
    [key: string]: { roles: string[] };
  };
  scope: string;
  email_verified: boolean;
  preferred_username: string;
  given_name: string;
  family_name: string;
}