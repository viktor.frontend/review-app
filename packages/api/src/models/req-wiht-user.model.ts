import { IKeycloakUser } from "./keycloak-user.model";

export interface RequestWithUser extends Request {
  user?: IKeycloakUser
}