import { graphqlConfig } from './graphql.config';
import { mysqlConfig } from './mysql.config';
import { keycloakConfig } from './keycloak.config';
import { amqpConfig } from './amqp';
import { elasticsearchConfig } from './elasticsearch';
import { registerAs } from '@nestjs/config';

export const configuration = [
  registerAs('graphql', () => () => graphqlConfig),
  registerAs('db', () => mysqlConfig),
  registerAs('keycloak', () => keycloakConfig),
  registerAs('amqp', () => amqpConfig),
  registerAs('elasticsearch', () => elasticsearchConfig),
];
