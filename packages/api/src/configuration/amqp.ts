export const amqpConfig = {
  hostname: 'mq-service',
  port: 5672,
  username: 'guest',
  password: 'guest',
  queuePrefix: 'dev',
  get connectionString() {
    return `amqp://${this.username}:${this.password}@${this.hostname}:${this.port}`;
  },
};

