export const keycloakConfig = {
  host: 'http://keycloak:8080',
  realm: 'review_app',
  clientId: 'portal',
};
