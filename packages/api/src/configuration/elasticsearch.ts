import {ElasticsearchModuleOptions} from '@nestjs/elasticsearch';

export const elasticsearchConfig: ElasticsearchModuleOptions = {
  node: 'http://elasticsearch:9200'
}