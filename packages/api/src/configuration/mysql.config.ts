import { TypeOrmModuleOptions } from "@nestjs/typeorm";

export const mysqlConfig: TypeOrmModuleOptions =  {
  type: 'mysql',
  host: 'mysql-db',
  port: 3306,
  username: 'guest',
  password: 'guest',
  database: 'review-app',
  synchronize: true,
  autoLoadEntities: true,
  migrations: ['migrations/**/*.ts'],
  migrationsRun: true,
  cli: {
    entitiesDir: 'src',
    migrationsDir: 'migrations',
  },
};