import { GqlModuleOptions } from "@nestjs/graphql";

export const graphqlConfig: GqlModuleOptions = {
  installSubscriptionHandlers: true,
  autoSchemaFile: 'schema.gql',
}