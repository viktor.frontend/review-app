import { Injectable, NestMiddleware } from '@nestjs/common';
import { IKeycloakUser } from 'src/models/keycloak-user.model';
import * as JWT from 'jsonwebtoken';
import { AuthService } from 'src/modules/auth/auth.service';
import { IKeycloakTokenData } from 'src/models/token-data.model';
import pick from 'lodash/pick';
import get from 'lodash/get';
import { RequestWithUser } from 'src/models/req-wiht-user.model';
import { ConfigService } from '@nestjs/config';

const AUTH_HEADER = 'authorization';
const BEARER = 'Bearer';

@Injectable()
export class AuthMiddleware implements NestMiddleware {
  constructor(
    private readonly configService: ConfigService,
    private readonly authService: AuthService
  ) {}

  async use(req: RequestWithUser, res: Response, next: Function) {
    if (req.headers && !req.headers[AUTH_HEADER]) {
      return next();
    }

    const token = this.getTokenWithoutBearer(req.headers[AUTH_HEADER]);

    if (!token) {
      return next();
    }

    const data = JWT.decode(token);
    const now = Date.now();
    const tokenExpires = data.exp * 1000;
    const tokenStarts = data.nbf * 1000;

    if (tokenExpires < now || now < tokenStarts) {
      return next();
    }

    const isTokenValidInKeycloak = await this.authService.isTokenValid(token);

    if (!isTokenValidInKeycloak) {
      return next();
    }

    req.user = this.extractUserFromTokenData(data);
    return next();
  }

  private getTokenWithoutBearer(token: string): string {
    return token.startsWith(BEARER) ? token.replace(`${BEARER} `, '') : token;
  }

  private extractUserFromTokenData(tokenData: IKeycloakTokenData): IKeycloakUser {
    const { clientId } = this.configService.get('keycloak');
    const user: IKeycloakUser = pick(tokenData, ['name', 'email']) as IKeycloakUser;
    user.keycloakId = tokenData.sub;
    user.roles = get(tokenData, `resource_access.${clientId}.roles`);
    return user;
  }
}
