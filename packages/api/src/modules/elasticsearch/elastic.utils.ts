import * as _ from 'lodash';
import { ApiResponse } from '@elastic/elasticsearch';

export const createFilterByTerm = (term: string) => (
  value: string | boolean,
) => ({
  term: {
    [term]: {
      value,
    },
  },
});

export const createPartialSearch = (field: string, value: string) => ({
  wildcard: {
    [`${field}`]: `*${value.toLowerCase()}*`,
  },
});

export const createMultifieldPartialSearch = (fields: string[], value: string) =>  ({
  should: fields.map(field => createPartialSearch(field, value)),
  minimum_should_match: 1, // eslint-disable-line
});


export const getHits = (hits: ApiResponse): any[] => _.get(hits, 'hits.hits')
export const getFirstHit = (hits: ApiResponse): any => _.first(getHits(hits));

export const getHitsSources = (hits: ApiResponse): any[] => getHits(hits).map(h => _.get(h, '_source'))
export const getFirstHitSource = (hits: ApiResponse): any => _.first(getHitsSources(hits));
