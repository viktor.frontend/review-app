import { Injectable } from '@nestjs/common';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import { createFilterByTerm, getFirstHit, getHitsSources, createMultifieldPartialSearch } from './elastic.utils';

const filterById = createFilterByTerm('id');

@Injectable()
export abstract class CustomElasticService<T> {
  constructor(
    private readonly elkService: ElasticsearchService,
    private readonly INDEX: string,
    private readonly TYPE: string,
  ) {}

  async searchByFields(term: string, fields: string[]): Promise<number[]> {
    const { body } = await this.elkService
      .search({
        type: this.TYPE,
        index: this.INDEX,
        body: {
          query: {
            bool: !!term ?  createMultifieldPartialSearch(fields, term) : {},
          },
          size: 1000,
        }
      });

    return getHitsSources(body).map(source => source.id);
  }

  async create(payload: T) {
    await this.elkService.index({
      type: this.TYPE,
      index: this.INDEX,
      body: payload,
    });
  }

  async update (payload: T) {
    const { id } = payload as any;
    const { body } = await this.elkService
      .search({
        type: this.TYPE,
        index: this.INDEX,
        body: {
          query: {
            bool: {
              filter: [filterById(`${id}`)],
            },
          },
          size: 1,
        },
      });
    const hit = getFirstHit(body);
    if (!hit) {
      return;
    }
    const _id = hit._id;
    await this.elkService
      .update({
        id: _id,
        type: this.TYPE,
        index: this.INDEX,
        body: {
          doc: payload,
        },
      });
  }

  async deleteById(id: number) {
    return this.elkService.deleteByQuery({
      type: this.TYPE,
      index: this.INDEX,
      body: {
        query: {
          bool: {
            filter: [filterById(`${id}`)],
          },
        },
        size: 1,
      },
    });
  }
}
