import { FaqService } from './faq.service';
import { Faq } from './models/faq.entity';
import { Query, Resolver, Args } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/guards/auth.guard';
import { User } from 'src/decorators/user.decorator';
import { IKeycloakUser } from 'src/models/keycloak-user.model';
import { FaqArgs } from './models/faq.dto';

@UseGuards(AuthGuard)
@Resolver(() => Faq)
export class FaqResolver {
  constructor(private readonly faqService: FaqService) {}
  
  @Query(() => [Faq])
  faqs(
    @Args() args: FaqArgs,
    @User() user: IKeycloakUser
  ): Promise<Faq[]> {
    return this.faqService.getAll({...args, roles: user.roles});
  }
}
