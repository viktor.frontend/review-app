import { Injectable } from '@nestjs/common';
import { CustomElasticService } from '../elasticsearch/elastic.service';
import { Faq } from './models/faq.entity';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import pick from 'lodash/pick';

const INDEX = 'faq';
const TYPE = 'doc';

const SEARCHABLE_FIELDS = ['title', 'content'];
const extractFaqDataForElastic = (faq: Faq) => pick(faq, ['id', ...SEARCHABLE_FIELDS]);

@Injectable()
export class FaqElasticService extends CustomElasticService<Partial<Faq>> {
  constructor(readonly elasticsearchService: ElasticsearchService) {
    super(elasticsearchService, INDEX, TYPE);
  }

  async search(term: string) {
    return await super.searchByFields(term, SEARCHABLE_FIELDS);
  }

  async create(faq: Faq) {
    const payload = extractFaqDataForElastic(faq);
    await super.create(payload);
  }

  async update(faq: Faq) {
    const payload = extractFaqDataForElastic(faq);
    await super.update(payload);
  }
}
