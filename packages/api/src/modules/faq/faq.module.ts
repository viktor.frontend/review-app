import { Module } from '@nestjs/common';
import { FaqResolver } from './faq.resolver';
import { FaqService } from './faq.service';
import { Faq } from './models/faq.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { FaqProvider } from './faq.provider';
import { ElasticsearchModule, ElasticsearchModuleAsyncOptions } from '@nestjs/elasticsearch';
import { ConfigService } from '@nestjs/config';
import { FaqElasticService } from './faq-elastic.service';
import { CategoryModule } from '../category/category.module';
import { RoleModule } from '../role/role.module';

@Module({
  imports: [
    ElasticsearchModule.registerAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => config.get('elasticsearch'),
    } as ElasticsearchModuleAsyncOptions),
    TypeOrmModule.forFeature([Faq]),
    CategoryModule,
    RoleModule,
  ],
  providers: [FaqResolver, FaqService, FaqProvider, FaqElasticService],
})
export class FaqModule {}
