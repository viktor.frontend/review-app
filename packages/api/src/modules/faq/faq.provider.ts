import { Injectable } from '@nestjs/common';
import { AMQPController } from '../amqp/amqp.controller';
import { ConfigService } from '@nestjs/config';
import { FaqService } from './faq.service';
import { ICreateFaq } from './models/faq.model';

const QUEUE = 'faqs';

@Injectable()
export class FaqProvider extends AMQPController<ICreateFaq> {
  constructor(readonly configService: ConfigService, private readonly faqService: FaqService) {
    super(configService.get('amqp').connectionString, QUEUE, QUEUE);
  }

  async handleCreate(payload: ICreateFaq) {
    await this.faqService.create(payload);
  }

  async handleUpdate(payload: ICreateFaq) {
    await this.faqService.update(payload);
  }

  async handleDelete(payload: Partial<ICreateFaq>) {
    await this.faqService.delete(payload);
  }
}
