import { Injectable } from '@nestjs/common';
import { Faq } from './models/faq.entity';
import { Repository, DeleteResult, getConnection } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { FaqElasticService } from './faq-elastic.service';
import { CategoryService } from '../category/category.service';
import { ICreateFaq } from './models/faq.model';
import { RoleService } from '../role/role.service';

interface IFaqFiltersOptions {
  categories?: number[];
  searchTerm?: string;
  roles: string[]
}

@Injectable()
export class FaqService {
  constructor(
    @InjectRepository(Faq)
    private readonly faqRepository: Repository<Faq>,
    private readonly faqElkService: FaqElasticService,
    private readonly categoryService: CategoryService,
    private readonly roleService: RoleService,
  ) {}

  async getAll({
    categories,
    searchTerm,
    roles = []
  } : IFaqFiltersOptions): Promise<Faq[]> {
    const isNoFiltersApplied = !searchTerm && (!categories || !categories.length)
    let searchedIds: number[] = [];
    if (searchTerm) {
      searchedIds = await this.faqElkService.search(searchTerm);
      if (!searchedIds.length) return []
    }
    
    let faqsQuery = getConnection()
      .getRepository(Faq)
      .createQueryBuilder("faq")
      .leftJoinAndSelect("faq.categories", "category")
      .leftJoinAndSelect("faq.roles", "role")
      .where("role.name IN (:...roles)", { roles })
      .andWhere("faq.showOnFaq = :isNoFiltersApplied", { isNoFiltersApplied })

    if (searchTerm && searchedIds) {
      faqsQuery = faqsQuery
        .andWhere("faq.id IN (:...searchedIds)", { searchedIds })
    }
    if (categories && categories.length) {
      faqsQuery = faqsQuery
        .andWhere("category.id IN (:...categories)", { categories })
    }
    return await faqsQuery.getMany();
  }

  async create(payload: ICreateFaq): Promise<Faq> { 
    const faq = await this.convertPayloadToEntity(payload);
    const faqRecord = this.faqRepository.create(faq);
    const result = await this.faqRepository.save(faqRecord);
    await this.faqElkService.create(faq);
    return result;
  }

  async update(payload: ICreateFaq): Promise<Faq> {
    const faq = await this.convertPayloadToEntity(payload);
    // Update doesn't work for entities with many-to-many relations
    // according to https://github.com/typeorm/typeorm/issues/2821
    const result = await this.faqRepository.save(faq);
    await this.faqElkService.update(faq);
    return result;
  }

  async delete(faq: Partial<ICreateFaq>): Promise<DeleteResult> {
    const result = await this.faqRepository.delete({ id: faq.id });
    await this.faqElkService.deleteById(faq.id);
    return result;
  }

  private async convertPayloadToEntity(payload: ICreateFaq): Promise<Faq> {
    const categories = await this.categoryService.getByIds(payload.categories);
    const roles = await this.roleService.getByIds(payload.roles);
    return {...payload, categories, roles};
  }
}
