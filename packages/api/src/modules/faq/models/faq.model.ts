export interface ICreateFaq {
  id: number;
  title: string;
  content: string;
  showOnTop: boolean;
  showOnFaq: boolean;
  categories: number[];
  roles: number[];
}