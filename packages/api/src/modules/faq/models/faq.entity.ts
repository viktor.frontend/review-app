import { PrimaryGeneratedColumn, Entity, Column, ManyToMany, JoinTable } from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';
import { Category } from '../../category/category.entity';
import { Role } from '../../role/role.entity';

@ObjectType()
@Entity()
export class Faq {
  // eslint-disable-next-line
  @Field(() => ID)
  @PrimaryGeneratedColumn()
  id: number;

  @Field()
  @Column()
  title: string;

  @Field()
  @Column({ type: 'longtext' })
  content: string;

  @Field()
  @Column({type: 'bool'})
  showOnFaq: boolean;

  @Field(() => Category)
  @ManyToMany(() => Category)
  @JoinTable()
  categories: Category[];

  @ManyToMany(() => Role)
  @JoinTable()
  roles: Role[];
}
