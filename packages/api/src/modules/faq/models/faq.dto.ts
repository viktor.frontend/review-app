import { ArgsType, Field, Int } from 'type-graphql';

@ArgsType()
export class FaqArgs {
  @Field(() => [Int], { nullable: true })
  categories?: number[];

  @Field({ nullable: true })
  searchTerm?: string;
}
