import { Module } from '@nestjs/common';
import { Role } from './role.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RoleService } from './role.service';

@Module({
  imports: [
    TypeOrmModule.forFeature([Role]),
  ],
  providers: [RoleService],
  exports: [RoleService]
})
export class RoleModule {}
