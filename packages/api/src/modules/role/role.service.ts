import { Injectable } from "@nestjs/common";
import { Role } from "./role.entity";
import { Repository, In } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
export class RoleService {
  constructor(
    @InjectRepository(Role) private readonly roleRepository: Repository<Role>,
  ) {}

  async getAll() {
    return await this.roleRepository.find();
  }

  async getByIds(ids: number[]) {
    return await this.roleRepository.find({where: {id: In(ids)}});
  }

}