import { SupportPageService } from './support-page.service';
import { SupportPage } from './models/support-page.entity';
import { Query, Resolver, Args } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/guards/auth.guard';
import { User } from 'src/decorators/user.decorator';
import { IKeycloakUser } from 'src/models/keycloak-user.model';
import { SupportPageArgs } from './models/support-page.dto';

@UseGuards(AuthGuard)
@Resolver(() => SupportPage)
export class SupportPageResolver {
  constructor(private readonly supportPageService: SupportPageService) {}
  
  @Query(() => [SupportPage])
  supportPages(
    @User() user: IKeycloakUser
  ): Promise<SupportPage[]> {
    return this.supportPageService.getAll({roles: user.roles});
  }


  @Query(() => SupportPage)
  async supportPage(@Args() args: SupportPageArgs) {
    return this.supportPageService.getOneBySlug(args.slug);
  }

}
