import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { SupportPage } from './models/support-page.entity';
import { Repository, DeleteResult, getConnection, In } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { SupportPagePayload } from './models/support-page.model';
import { RoleService } from '../role/role.service';
import { validateOrReject } from 'class-validator';
import { TooltileService } from '../tooltiles/tooltile.service';


@Injectable()
export class SupportPageService {
  constructor(
    @InjectRepository(SupportPage)
    private readonly supportPageRepository: Repository<SupportPage>,
    private readonly roleService: RoleService,
    private readonly tooltileService: TooltileService,
  ) {}

  async getAll({
    roles = []
  }): Promise<SupportPage[]> {
    return await getConnection()
      .getRepository(SupportPage)
      .createQueryBuilder("supportPage")
      .leftJoinAndSelect("supportPage.roles", "role")
      .where("role.name IN (:...roles)", { roles })
      .getMany();
  }

  async getByIds(ids: number[]) {
    return await this.supportPageRepository.find({where: {id: In(ids)}});
  }

  async getOneBySlug(slug: string) {
    return getConnection()
      .getRepository(SupportPage)
      .createQueryBuilder("supportPage")
      .where("supportPage.slug = :slug", {slug})
      .leftJoinAndSelect("supportPage.relatedSupportPages", "relatedSupportPages")
      .leftJoinAndSelect("supportPage.tooltiles", "tooltile")
      .leftJoinAndSelect("tooltile.categories", "tilecategory")
      .getOne();
  }

  async create(payload: SupportPagePayload): Promise<SupportPage> { 
    const SupportPage = await this.convertPayloadToEntity(payload);
    const supportPageRecord = this.supportPageRepository.create(SupportPage);
    const result = await this.supportPageRepository.save(supportPageRecord);
    return result;
  }

  async update(payload: SupportPagePayload): Promise<SupportPage> {
    return await this.supportPageRepository.save(await this.convertPayloadToEntity(payload));
  }

  async delete(supportPage: Partial<SupportPagePayload>): Promise<DeleteResult> {
    return await this.supportPageRepository.delete({ id: supportPage.id });
  }

  private async convertPayloadToEntity(payload: SupportPagePayload): Promise<SupportPage> {
    const instance = new SupportPagePayload(payload);
    try {
      await validateOrReject(instance);
    } catch (error) {
      console.error(error);
      throw new UnprocessableEntityException(error)
    }
    const roles = await this.roleService.getByIds(payload.roles);
    const tooltiles = payload.relatedSupportPages.length ? await this.tooltileService.getByIds(payload.tooltiles) : [];
    const relatedSupportPages = payload.relatedSupportPages.length ? await this.getByIds(payload.relatedSupportPages) : [];
    return {...payload, roles, relatedSupportPages, tooltiles};
  }
}
