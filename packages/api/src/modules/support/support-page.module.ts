import { Module } from '@nestjs/common';
import { SupportPageResolver } from './support-page.resolver';
import { SupportPageService } from './support-page.service';
import { SupportPage } from './models/support-page.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { SupportPageProvider } from './support-page.provider';
import { RoleModule } from '../role/role.module';
import { TooltilesModule } from '../tooltiles/tooltile.module';

@Module({
  imports: [
    TypeOrmModule.forFeature([SupportPage]),
    TooltilesModule,
    RoleModule,
  ],
  providers: [SupportPageResolver, SupportPageService, SupportPageProvider],
})
export class SupportPageModule {}
