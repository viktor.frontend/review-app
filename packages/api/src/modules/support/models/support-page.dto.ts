import { ArgsType, Field } from 'type-graphql';

@ArgsType()
export class SupportPageArgs {
  @Field(() => String)
  slug: string;
}
