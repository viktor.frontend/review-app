import { PrimaryGeneratedColumn, Entity, Column, ManyToMany, JoinTable } from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';
import { Role } from '../../role/role.entity';
import { Tooltile } from 'src/modules/tooltiles/tooltile.entity';

@ObjectType()
@Entity()
export class SupportPage {
  @PrimaryGeneratedColumn()
  @Field(() => ID)
  id: number;

  @Column()
  @Field()
  title: string;

  @Column({ type: 'longtext' })
  @Field()
  content: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  image?: string;

  @Column()
  @Field()
  created: Date;

  @Column()
  @Field()
  updated: Date;

  @Column({unique: true})
  @Field()
  slug: string;

  @Field(() => [SupportPage], {nullable: true})
  @ManyToMany(() => SupportPage)
  @JoinTable()
  relatedSupportPages: SupportPage[];

  @Field(() => [Tooltile], {nullable: true})
  @ManyToMany(() => Tooltile)
  @JoinTable()
  tooltiles: Tooltile[];

  @ManyToMany(() => Role)
  @JoinTable()
  roles: Role[];
}
