import { IsInt, IsString, Length, IsArray, ArrayUnique, IsDate, ArrayMinSize } from 'class-validator';

export class SupportPagePayload {
  @IsInt()
  id: number;

  @IsString()
  @Length(10)
  title: string;

  @IsString()
  @Length(10)
  content: string;

  image?: string;

  @IsDate()
  created: Date;

  @IsDate()
  updated: Date;

  @IsString()
  @Length(1)
  slug: string;

  @IsArray()
  @ArrayUnique()
  @IsInt({ each: true })
  relatedSupportPages: number[];

  @IsArray()
  @ArrayUnique()
  @IsInt({ each: true })
  tooltiles: number[];

  @IsArray()
  @ArrayMinSize(1)
  @ArrayUnique()
  @IsInt({ each: true })
  roles: number[];

  constructor(initialData: Record<string, any>) {
    this.id = initialData.id;
    this.title = initialData.title;
    this.content = initialData.content;
    this.image = initialData.image;
    this.created = new Date(initialData.created);
    this.updated = initialData.updated ? new Date(initialData.updated) : new Date();
    this.slug = initialData.slug;
    this.tooltiles = initialData.tooltiles ? initialData.tooltiles : [];
    this.relatedSupportPages = initialData.relatedSupportPages ? initialData.relatedSupportPages : [];
    this.roles = initialData.roles ? initialData.roles : [];
  }
}
