import { Injectable } from '@nestjs/common';
import { AMQPController } from '../amqp/amqp.controller';
import { ConfigService } from '@nestjs/config';
import { SupportPageService } from './support-page.service';
import { SupportPagePayload } from './models/support-page.model';

const QUEUE = 'support-pages';

@Injectable()
export class SupportPageProvider extends AMQPController<SupportPagePayload> {
  constructor(readonly configService: ConfigService, private readonly supportPageService: SupportPageService) {
    super(configService.get('amqp').connectionString, QUEUE, QUEUE);
  }

  async handleCreate(payload: SupportPagePayload) {
    await this.supportPageService.create(payload);
  }

  async handleUpdate(payload: SupportPagePayload) {
    await this.supportPageService.update(payload);
  }

  async handleDelete(payload: Partial<SupportPagePayload>) {
    await this.supportPageService.delete(payload);
  }
}
