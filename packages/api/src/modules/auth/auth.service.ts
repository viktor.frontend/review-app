import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Token } from './token.entity';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import axios from 'axios';
import * as JWT from 'jsonwebtoken';

@Injectable()
export class AuthService {
  keycloakUrl: string;

  constructor(
    @InjectRepository(Token)
    private readonly tokenRepository: Repository<Token>,
    private readonly configService: ConfigService,
  ) {
    const { host, realm } = this.configService.get('keycloak');
    this.keycloakUrl = `${host}/auth/realms/${realm}/protocol/openid-connect/userinfo`;
  }

  async isTokenValid(token: string): Promise<boolean> {
    if (await this.isTokenSavedInDB(token)) {
      return true;
    }

    if (await this.isTokenValidInKeycloak(token)) {
      await this.saveTokenToDB(token);
      return true;
    }

    return false;
  }

  private async isTokenSavedInDB(token: string): Promise<boolean> {
    return !!(await this.tokenRepository.count({
      where: { token },
    }));
  }

  private async isTokenValidInKeycloak(token: string): Promise<boolean> {
    try {
      const response = await axios({
        method: 'GET',
        url: this.keycloakUrl,
        headers: {
          Authorization: `Bearer ${token}`,
        },
      });
      return response.status === 200;
    } catch (error) {
      return false;
    }
  }

  private async saveTokenToDB(token: string) {
    const tokenData = JWT.decode(token);
    const entity = {
      token,
      keycloakId: tokenData.sub,
      expiresAt: new Date(tokenData.exp * 1000),
    };
    const tokenRecord = this.tokenRepository.create(entity);
    return await this.tokenRepository.save(tokenRecord);
  }
}
