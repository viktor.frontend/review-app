import { Column, Entity, Index, PrimaryGeneratedColumn } from 'typeorm';

@Entity()
export class Token {
  @PrimaryGeneratedColumn()
  id: number;

  @Index()
  @Column()
  keycloakId: string;

  @Index()
  @Column({ type: "longtext" })
  token: string;

  @Column({ type: 'datetime' })
  expiresAt: Date;
}
