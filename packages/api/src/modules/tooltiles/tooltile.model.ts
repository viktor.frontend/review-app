import { IsInt, IsString, Length, IsBoolean, IsArray, ArrayUnique, IsDate, ArrayMinSize, IsUrl } from 'class-validator';

export class TooltilePayload {
  @IsInt()
  id: number;

  @IsString()
  @Length(5)
  title: string;

  image?: string;

  @IsUrl()
  url: string;

  @IsDate()
  startDate: Date;

  @IsBoolean()
  active: boolean;

  @IsInt()
  dashboardCategory: number;

  @IsArray()
  @ArrayMinSize(1)
  @ArrayUnique()
  categories: number[];

  @IsArray()
  @ArrayMinSize(1)
  @ArrayUnique()
  roles: number[];

  constructor(initialData: Record<string, any>) {
    this.id = initialData.id;
    this.title = initialData.title;
    this.image = initialData.image;
    this.url = initialData.url;
    this.startDate = new Date(initialData.startDate);
    this.active = initialData.active;
    this.dashboardCategory = initialData.dashboardCategory;
    this.categories = initialData.categories ? initialData.categories : [];
    this.roles = initialData.roles ? initialData.roles : [];
  }
}
