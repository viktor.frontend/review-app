import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Tooltile } from './tooltile.entity';
import { CategoryModule } from '../category/category.module';
import { RoleModule } from '../role/role.module';
import { TooltileProvider } from './tooltile.provider';
import { TooltileService } from './tooltile.service';
import { DashboardCategoryModule } from '../dashboard-category/dashboard-category.module';
import { TooltileResolver } from './tooltile.resolver';

@Module({
  imports: [
    TypeOrmModule.forFeature([Tooltile]),
    DashboardCategoryModule,
    CategoryModule,
    RoleModule,
  ],
  providers: [TooltileResolver, TooltileService, TooltileProvider], 
  exports: [TooltileService], 
})
export class TooltilesModule {}
