import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { Repository, DeleteResult, getConnection, In } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { CategoryService } from '../category/category.service';
import { RoleService } from '../role/role.service';
import { validateOrReject } from 'class-validator';
import { Tooltile } from './tooltile.entity';
import { TooltilePayload } from './tooltile.model';
import { DashboardCategoryService } from '../dashboard-category/dashboard-category.service';

@Injectable()
export class TooltileService {
  constructor(
    @InjectRepository(Tooltile)
    private readonly tooltileRepository: Repository<Tooltile>,
    private readonly categoryService: CategoryService,
    private readonly dashboardCategoryService: DashboardCategoryService,
    private readonly roleService: RoleService,
  ) {}

  async findAll({
    roles = []
  }): Promise<Tooltile[]> {
    return await getConnection()
      .getRepository(Tooltile)
      .createQueryBuilder("tooltile")
      .leftJoinAndSelect("tooltile.categories", "category")
      .leftJoinAndSelect("tooltile.roles", "role")
      .where("role.name IN (:...roles)", { roles })
      .getMany()
  }

  async getByIds(ids: number[]) {
    return await this.tooltileRepository.find({where: {id: In(ids), relations: ['categories']}});
  }

  async create(payload: TooltilePayload): Promise<Tooltile> { 
    const tooltileItem = await this.convertPayloadToEntity(payload);
    const tooltileRecord = this.tooltileRepository.create(tooltileItem);
    const result = await this.tooltileRepository.save(tooltileRecord);
    return result;
  }

  async update(payload: TooltilePayload): Promise<Tooltile> {
    const tooltile = await this.convertPayloadToEntity(payload);
    const result = await this.tooltileRepository.save(tooltile);
    return result;
  }

  async delete(tooltile: Partial<TooltilePayload>): Promise<DeleteResult> {
    const result = await this.tooltileRepository.delete({ id: tooltile.id });
    return result;
  }

  private async convertPayloadToEntity(payload: TooltilePayload): Promise<Tooltile> {
    const instance = new TooltilePayload(payload);
    try {
      await validateOrReject(instance);
    } catch (error) {
      console.error(error);
      throw new UnprocessableEntityException(error)
    }
    const categories = await this.categoryService.getByIds(payload.categories);
    const roles = await this.roleService.getByIds(payload.roles);
    const dashboardCategory = await this.dashboardCategoryService.findById(payload.dashboardCategory);
    return {...payload, categories, roles, dashboardCategory};
  }
}
