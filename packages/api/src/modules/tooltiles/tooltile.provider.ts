import { Injectable } from '@nestjs/common';
import { AMQPController } from '../amqp/amqp.controller';
import { ConfigService } from '@nestjs/config';
import { TooltileService } from './tooltile.service';
import { TooltilePayload } from './tooltile.model';

const QUEUE = 'tooltiles';

@Injectable()
export class TooltileProvider extends AMQPController<TooltilePayload> {
  constructor(readonly configService: ConfigService, private readonly tooltileService: TooltileService) {
    super(configService.get('amqp').connectionString, QUEUE, QUEUE);
  }

  async handleCreate(payload: TooltilePayload) {
    await this.tooltileService.create(payload);
  }

  async handleUpdate(payload: TooltilePayload) {
    await this.tooltileService.update(payload);
  }

  async handleDelete(payload: Partial<TooltilePayload>) {
    await this.tooltileService.delete(payload);
  }
}
