import { Column, Entity, PrimaryColumn, ManyToMany, JoinTable, ManyToOne } from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';
import { Category } from '../category/category.entity';
import { Role } from '../role/role.entity';
import { DashboardCategory } from '../dashboard-category/dashboard-category.entity';

@ObjectType()
@Entity()
export class Tooltile {
  @Field(() => ID)
  @PrimaryColumn()
  id: number;

  @Field()
  @Column()
  title: string;

  @Field({ nullable: true })
  @Column({ nullable: true })
  image?: string;

  @Field()
  @Column()
  url: string;

  @Field()
  @Column()
  startDate: Date;

  @Field()
  @Column()
  active: boolean;
  
  @Field(() => DashboardCategory)
  @ManyToOne(() => DashboardCategory, category => category.tooltiles)
  dashboardCategory: DashboardCategory;

  @Field(() => Category)
  @ManyToMany(() => Category)
  @JoinTable()
  categories: Category[];

  @ManyToMany(() => Role)
  @JoinTable()
  roles: Role[];
}
