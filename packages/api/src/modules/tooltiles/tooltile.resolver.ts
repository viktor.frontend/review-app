import { Query, Resolver } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { Tooltile } from './tooltile.entity';
import { TooltileService } from './tooltile.service';
import { AuthGuard } from 'src/guards/auth.guard';
import { User } from 'src/decorators/user.decorator';
import { IKeycloakUser } from 'src/models/keycloak-user.model';

@UseGuards(AuthGuard)
@Resolver(() => Tooltile)
export class TooltileResolver {
  constructor(
    private readonly tooltileService: TooltileService,
  ) {}

  @Query(() => [Tooltile])
  tooltiles(@User() user: IKeycloakUser): Promise<Tooltile[]> {
    return this.tooltileService.findAll({roles: user.roles});
  }
}
