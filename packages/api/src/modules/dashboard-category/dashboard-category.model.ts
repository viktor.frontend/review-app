import { IsInt, IsString } from 'class-validator';
import { Tooltile } from '../tooltiles/tooltile.entity';

export class DashboardCategoryPayload {
  @IsInt()
  id: number;

  @IsString()
  name: string;

  @IsInt()
  order: number;

  tooltiles: Tooltile[]

  constructor(initialData: Record<string, any>) {
    this.id = initialData.id;
    this.name = initialData.name;
    this.order = initialData.order;
    this.tooltiles = [];
  }
}
