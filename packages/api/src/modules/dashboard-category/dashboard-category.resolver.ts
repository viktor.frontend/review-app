import { Query, Resolver, ResolveProperty, Parent } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { DashboardCategory } from './dashboard-category.entity';
import { DashboardCategoryService } from './dashboard-category.service';
import { AuthGuard } from 'src/guards/auth.guard';
import { Tooltile } from '../tooltiles/tooltile.entity';

@UseGuards(AuthGuard)
@Resolver(() => DashboardCategory)
export class DashboardCategoryResolver {
  constructor(
    private readonly dashboardCategoryService: DashboardCategoryService
  ) {}

  @Query(() => [DashboardCategory])
  dashboardCategories(): Promise<DashboardCategory[]> {
    return this.dashboardCategoryService.findAll();
  }


  @ResolveProperty('tooltiles', () => [Tooltile], )
  products(@Parent() { id }: DashboardCategory) {
    return this.dashboardCategoryService.findTiles(id);
  }
}
