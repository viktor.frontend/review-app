import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository, DeleteResult } from 'typeorm';
import { DashboardCategory } from './dashboard-category.entity';
import { DashboardCategoryPayload } from './dashboard-category.model';
import { validateOrReject } from 'class-validator';

@Injectable()
export class DashboardCategoryService {
  constructor(
    @InjectRepository(DashboardCategory)
    private readonly dashboardCategoryRepository: Repository<DashboardCategory>,
  ) {}

  findAll(): Promise<DashboardCategory[]> {
    return this.dashboardCategoryRepository.find({
      order: { order: 'ASC' },
    });
  }

  findById(id: number): Promise<DashboardCategory> {
    return this.dashboardCategoryRepository.findOne({ where: {id} });
  }

  async findTiles(id: number) {
    const { tooltiles } = await this.dashboardCategoryRepository.findOne({
      where: { id },
      relations: ['tooltiles', 'tooltiles.categories'],
    });
    return tooltiles;
  }

  async create(payload: DashboardCategory) {
    const category = await this.convertPayloadToEntity(payload);
    const record = this.dashboardCategoryRepository.create(category);
    return this.dashboardCategoryRepository.save(record);
  }

  async update(payload: DashboardCategory) {
    const category = await this.convertPayloadToEntity(payload);
    delete category.tooltiles;
    return this.dashboardCategoryRepository.save(category);
  }

  delete(category: Partial<DashboardCategory>): Promise<DeleteResult> {
    return this.dashboardCategoryRepository.delete({ id: category.id });
  }


  private async convertPayloadToEntity(payload: DashboardCategoryPayload): Promise<DashboardCategory> {
    const instance = new DashboardCategoryPayload(payload);
    try {
      await validateOrReject(instance);
    } catch (error) {
      console.error(error);
      throw new UnprocessableEntityException(error)
    }
    return payload;
  }
}
