import { Column, Entity, PrimaryColumn, OneToMany } from 'typeorm';
import { Field, ID, Int, ObjectType } from 'type-graphql';
import { Tooltile } from '../tooltiles/tooltile.entity';

@ObjectType()
@Entity()
export class DashboardCategory {
  @Field(() => ID)
  @PrimaryColumn()
  id: number;

  @Field()
  @Column()
  name: string;

  @Field(() => Int)
  @Column({type: 'int', unique: true})
  order: number;

  @Field(() => [Tooltile])
  @OneToMany(() => Tooltile, tooltile => tooltile.dashboardCategory)
  tooltiles: Tooltile[];
}
