import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { DashboardCategoryService } from './dashboard-category.service';
import { DashboardCategoryResolver } from './dashboard-category.resolver';
import { DashboardCategory } from './dashboard-category.entity';
import { DashboardCategoryProvider } from './dashboard-category.provider';

@Module({
  imports: [TypeOrmModule.forFeature([DashboardCategory])],
  providers: [
    DashboardCategoryResolver,
    DashboardCategoryService,
    DashboardCategoryProvider,
  ],
  exports: [DashboardCategoryService],
})
export class DashboardCategoryModule {}
