import { Injectable } from '@nestjs/common';
import { AMQPController } from '../amqp/amqp.controller';
import { ConfigService } from '@nestjs/config';
import { DashboardCategoryService } from './dashboard-category.service';
import { DashboardCategory } from './dashboard-category.entity';

const QUEUE = 'dashboard-categories';

@Injectable()
export class DashboardCategoryProvider extends AMQPController<DashboardCategory> {
  constructor(readonly configService: ConfigService, private readonly dashboardCategoryService: DashboardCategoryService) {
    super(configService.get('amqp').connectionString, QUEUE, QUEUE);
  }

  async handleCreate(payload: DashboardCategory) {
    await this.dashboardCategoryService.create(payload);
  }

  async handleUpdate(payload: DashboardCategory) {
    await this.dashboardCategoryService.update(payload);
  }

  async handleDelete(payload: Partial<DashboardCategory>) {
    await this.dashboardCategoryService.delete(payload);
  }
}

