
export enum AmqpMessageTypeEnum {
  CREATE = "CREATE",
  UPDATE = "UPDATE",
  DELETE = "DELETE",
}

export interface IAmqpMessage<T> {
  body: IAmqpMessageBody<T>;
  ack: () => void;
  nack: () => void;
}
export interface IAmqpMessageBody<T> {
  payload: T;
  type: AmqpMessageTypeEnum;
}