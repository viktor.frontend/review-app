import { Injectable } from '@nestjs/common';
import * as Amqp from 'amqp-ts';
import { Subject, Observable } from 'rxjs';
import { finalize } from 'rxjs/operators';
import { IAmqpMessage, IAmqpMessageBody, AmqpMessageTypeEnum } from './amqp.model';

@Injectable()
export abstract class AMQPController<T> {
  private connection: Amqp.Connection;
  private queue$: Observable<IAmqpMessage<T>>;

  constructor(connectionString: string, private queueName: string, private exchangeName: string) {
    this.connection = new Amqp.Connection(connectionString, null, {
      retries: 10,
      interval: 2000,
    });
    this.queue$ = this.createQueueObservable();
    this.queue$.subscribe(async message => {
      await this.listener(message);
    });
  }

  createQueueObservable(): Observable<IAmqpMessage<T>> {
    const consumerSubject: Subject<IAmqpMessage<T>> = new Subject();
    const exchange = this.connection.declareExchange(this.exchangeName);
    const queue = this.connection.declareQueue(this.queueName);

    queue.bind(exchange);

    queue.activateConsumer(message => {
      try {
        const body = JSON.parse(message.getContent()) as IAmqpMessageBody<T>;
        consumerSubject.next({
          body,
          ack: () => message.ack(),
          nack: () => message.nack(false, false),
        });
      } catch (err) {
        console.error(err);
        message.nack(false, false);
      }
    });

    consumerSubject.pipe(finalize(queue.stopConsumer));

    return consumerSubject.asObservable();
  }

  private async listener(message: IAmqpMessage<T>) {
    const handlersMap = this.getHandlersMap();
    const { type, payload } = message.body;

    if (!type || !payload || !handlersMap[type]) {
      message.nack();
    }

    try {
      await handlersMap[type](payload);
      message.ack();
    } catch (e) {
      console.error(e);
      message.nack();
    }
  }

  // eslint-disable-next-line
  async handleCreate(payload: T) {
    throw new Error('Method "handleCreate" is not implemented');
  }
  // eslint-disable-next-line
  async handleUpdate(payload: T) {
    throw new Error('Method "handleUpdate" is not implemented');
  }
  // eslint-disable-next-line
  async handleDelete(payload: T) {
    throw new Error('Method "handleDelete" is not implemented');
  }

  private getHandlersMap() {
    return {
      [AmqpMessageTypeEnum.CREATE]: this.handleCreate.bind(this),
      [AmqpMessageTypeEnum.UPDATE]: this.handleUpdate.bind(this),
      [AmqpMessageTypeEnum.DELETE]: this.handleDelete.bind(this),
    };
  }
}
