import { Module, MiddlewareConsumer, RequestMethod } from '@nestjs/common';
import { AuthModule } from '../auth/auth.module';
import { FaqModule } from '../faq/faq.module';
import { GraphQLModule } from '@nestjs/graphql';
import { TypeOrmModule } from '@nestjs/typeorm';
import { ConfigModule } from '@nestjs/config';

import { graphqlConfig } from '../../configuration/graphql.config';
import { mysqlConfig } from '../../configuration/mysql.config';
import { configuration } from 'src/configuration/configuration';
import { AuthMiddleware } from 'src/middlewares/auth.middleware';
import { CategoryModule } from '../category/category.module';
import { RoleModule } from '../role/role.module';
import { NewsModule } from '../news/news.module';
import { DashboardCategoryModule } from '../dashboard-category/dashboard-category.module';
import { TooltilesModule } from '../tooltiles/tooltile.module';
import { SupportPageModule } from '../support/support-page.module';

@Module({
  imports: [
    ConfigModule.forRoot({
      isGlobal: true,
      load: configuration,
    }),
    AuthModule,
    FaqModule,
    CategoryModule,
    RoleModule,
    NewsModule,
    DashboardCategoryModule,
    TooltilesModule,
    SupportPageModule,
    GraphQLModule.forRoot({ ...graphqlConfig, context: ({ req }) => ({ req }) }),
    TypeOrmModule.forRoot(mysqlConfig),
  ],
})
export class AppModule {
  configure(consumer: MiddlewareConsumer) {
    consumer.apply(AuthMiddleware).forRoutes({ path: '*', method: RequestMethod.ALL });
  }
}
