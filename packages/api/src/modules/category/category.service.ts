import { Injectable } from "@nestjs/common";
import { Category } from "./category.entity";
import { Repository, In } from "typeorm";
import { InjectRepository } from "@nestjs/typeorm";

@Injectable()
export class CategoryService {
  constructor(
    @InjectRepository(Category) private readonly categoryRepository: Repository<Category>,
  ) {}

  async getAll() {
    return await this.categoryRepository.find();
  }

  async getByIds(ids: number[]) {
    return await this.categoryRepository.find({where: {id: In(ids)}});
  }

}