import { UseGuards } from "@nestjs/common";
import { AuthGuard } from "src/guards/auth.guard";
import { Resolver, Query } from "@nestjs/graphql";
import { CategoryService } from "./category.service";
import { Category } from "./category.entity";

@UseGuards(AuthGuard)
@Resolver(() => Category)
export class CategoryResolver {
  constructor(private readonly categoryService: CategoryService) {}

  @Query(() => [Category])
  categories(): Promise<Category[]> {
    return this.categoryService.getAll();
  }
}
