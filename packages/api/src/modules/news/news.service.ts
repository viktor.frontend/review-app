import { Injectable, UnprocessableEntityException } from '@nestjs/common';
import { News } from './models/news.entity';
import { Repository, DeleteResult, getConnection, In } from 'typeorm';
import { InjectRepository } from '@nestjs/typeorm';
import { NewsElasticService } from './news-elastic.service';
import { CategoryService } from '../category/category.service';
import { NewsPayload } from './models/news.model';
import { RoleService } from '../role/role.service';
import { validateOrReject } from 'class-validator';
import { NewsList } from './models/news-output.dto';
import { TooltileService } from '../tooltiles/tooltile.service';

interface INewsFiltersOptions {
  from: number;
  size: number;
  categories?: number[];
  searchTerm?: string;
  roles: string[]
}

@Injectable()
export class NewsService {
  constructor(
    @InjectRepository(News)
    private readonly newsRepository: Repository<News>,
    private readonly newsElkService: NewsElasticService,
    private readonly categoryService: CategoryService,
    private readonly roleService: RoleService,
    private readonly tooltileService: TooltileService,
  ) {}

  async getAll({
    from = 0,
    size = 5,
    categories,
    searchTerm,
    roles = []
  } : INewsFiltersOptions): Promise<NewsList> {
    let searchedIds: number[] = [];
    if (searchTerm) {
      searchedIds = await this.newsElkService.search(searchTerm);
      if (!searchedIds.length) return {count: 0, items: []}
    }
    
    let newsQuery = getConnection()
      .getRepository(News)
      .createQueryBuilder("news")
      .leftJoinAndSelect("news.categories", "category")
      .leftJoinAndSelect("news.roles", "role")
      .where("role.name IN (:...roles)", { roles })

    if (searchTerm && searchedIds) {
      newsQuery = newsQuery
        .andWhere("news.id IN (:...searchedIds)", { searchedIds })
    }
    if (categories) {
      newsQuery = newsQuery
        .andWhere("category.id IN (:...categories)", { categories })
    }
    newsQuery
    return {
      count: await newsQuery.getCount(),
      items: await newsQuery.take(size).skip(from).getMany(),
    };
  }

  async getByIds(ids: number[]) {
    return await this.newsRepository.find({where: {id: In(ids)}});
  }

  async getOneBySlug(slug: string) {
    return getConnection()
      .getRepository(News)
      .createQueryBuilder("news")
      .where("news.slug = :slug", {slug})
      .leftJoinAndSelect("news.categories", "category")
      .leftJoinAndSelect("news.relatedNews", "relatedNews")
      .leftJoinAndSelect("news.tooltiles", "tooltile")
      .leftJoinAndSelect("tooltile.categories", "tilecategory")
      .getOne();
  }

  async create(payload: NewsPayload): Promise<News> { 
    const newsItem = await this.convertPayloadToEntity(payload);
    const newsRecord = this.newsRepository.create(newsItem);
    const result = await this.newsRepository.save(newsRecord);
    await this.newsElkService.create(newsItem);
    return result;
  }

  async update(payload: NewsPayload): Promise<News> {
    const news = await this.convertPayloadToEntity(payload);
    // Update doesn't work for entities with many-to-many relations
    // according to https://github.com/typeorm/typeorm/issues/2821
    const result = await this.newsRepository.save(news);
    await this.newsElkService.update(news);
    return result;
  }

  async delete(news: Partial<NewsPayload>): Promise<DeleteResult> {
    const result = await this.newsRepository.delete({ id: news.id });
    await this.newsElkService.deleteById(news.id);
    return result;
  }

  private async convertPayloadToEntity(payload: NewsPayload): Promise<News> {
    const instance = new NewsPayload(payload);
    try {
      await validateOrReject(instance);
    } catch (error) {
      console.error(error);
      throw new UnprocessableEntityException(error)
    }
    const categories = await this.categoryService.getByIds(payload.categories);
    const roles = await this.roleService.getByIds(payload.roles);
    const tooltiles = payload.relatedNews.length ? await this.tooltileService.getByIds(payload.tooltiles) : [];
    const relatedNews = payload.relatedNews.length ? await this.getByIds(payload.relatedNews) : [];
    return {...payload, categories, roles, relatedNews, tooltiles};
  }
}
