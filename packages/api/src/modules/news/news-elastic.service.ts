import { Injectable } from '@nestjs/common';
import { CustomElasticService } from '../elasticsearch/elastic.service';
import { News } from './models/news.entity';
import { ElasticsearchService } from '@nestjs/elasticsearch';
import pick from 'lodash/pick';

const INDEX = 'news';
const TYPE = 'doc';

const SEARCHABLE_FIELDS = ['title', 'content'];
const extractNewsDataForElastic = (news: News) => pick(news, ['id', ...SEARCHABLE_FIELDS]);

@Injectable()
export class NewsElasticService extends CustomElasticService<Partial<News>> {
  constructor(readonly elasticsearchService: ElasticsearchService) {
    super(elasticsearchService, INDEX, TYPE);
  }

  async search(term: string) {
    return await super.searchByFields(term, SEARCHABLE_FIELDS);
  }

  async create(news: News) {
    const payload = extractNewsDataForElastic(news);
    await super.create(payload);
  }

  async update(news: News) {
    const payload = extractNewsDataForElastic(news);
    await super.update(payload);
  }
}
