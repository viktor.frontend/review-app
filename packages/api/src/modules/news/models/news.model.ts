import { IsInt, IsString, Length, IsBoolean, IsArray, ArrayUnique, IsDate, ArrayMinSize } from 'class-validator';

export class NewsPayload {
  @IsInt()
  id: number;

  @IsString()
  @Length(10)
  title: string;

  @IsString()
  @Length(10, 300)
  shortContent: string;

  @IsString()
  @Length(10)
  content: string;

  image?: string;

  @IsDate()
  created: Date;

  @IsDate()
  updated: Date;

  @IsString()
  @Length(1)
  slug: string;

  @IsBoolean()
  showOnTop: boolean;

  @IsArray()
  @ArrayUnique()
  @IsInt({ each: true })
  relatedNews: number[];

  @IsArray()
  @ArrayUnique()
  @IsInt({ each: true })
  tooltiles: number[];

  @IsArray()
  @ArrayMinSize(1)
  @ArrayUnique()
  @IsInt({ each: true })
  categories: number[];

  @IsArray()
  @ArrayMinSize(1)
  @ArrayUnique()
  @IsInt({ each: true })
  roles: number[];

  constructor(initialData: Record<string, any>) {
    this.id = initialData.id;
    this.title = initialData.title;
    this.shortContent = initialData.shortContent;
    this.content = initialData.content;
    this.image = initialData.image;
    this.created = new Date(initialData.created);
    this.updated = initialData.updated ? new Date(initialData.updated) : new Date();
    this.slug = initialData.slug;
    this.showOnTop = initialData.showOnTop;
    this.tooltiles = initialData.tooltiles ? initialData.tooltiles : [];
    this.relatedNews = initialData.relatedNews ? initialData.relatedNews : [];
    this.categories = initialData.categories ? initialData.categories : [];
    this.roles = initialData.roles ? initialData.roles : [];
  }
}
