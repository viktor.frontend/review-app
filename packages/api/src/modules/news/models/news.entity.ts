import { PrimaryGeneratedColumn, Entity, Column, ManyToMany, JoinTable } from 'typeorm';
import { Field, ID, ObjectType } from 'type-graphql';
import { Category } from '../../category/category.entity';
import { Role } from '../../role/role.entity';
import { Tooltile } from 'src/modules/tooltiles/tooltile.entity';

@ObjectType()
@Entity()
export class News {
  @PrimaryGeneratedColumn()
  @Field(() => ID)
  id: number;

  @Column()
  @Field()
  title: string;

  @Column({ type: 'longtext' })
  @Field()
  shortContent: string;

  @Column({ type: 'longtext' })
  @Field()
  content: string;

  @Column({ nullable: true })
  @Field({ nullable: true })
  image?: string;

  @Column()
  @Field()
  created: Date;

  @Column()
  @Field()
  updated: Date;

  @Column({unique: true})
  @Field()
  slug: string;

  @Column()
  @Field()
  showOnTop: boolean;

  @Field(() => [News], {nullable: true})
  @ManyToMany(() => News)
  @JoinTable()
  relatedNews: News[];

  @Field(() => [Tooltile], {nullable: true})
  @ManyToMany(() => Tooltile)
  @JoinTable()
  tooltiles: Tooltile[];

  @Field(() => [Category])
  @ManyToMany(() => Category)
  @JoinTable()
  categories: Category[];

  @ManyToMany(() => Role)
  @JoinTable()
  roles: Role[];
}
