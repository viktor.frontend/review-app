import { ArgsType, Field } from 'type-graphql';

@ArgsType()
export class NewsItemArgs {
  @Field(() => String)
  slug: string;
}
