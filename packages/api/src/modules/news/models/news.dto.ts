import { ArgsType, Field, Int } from 'type-graphql';

@ArgsType()
export class NewsArgs {
  @Field(() => Int, { nullable: true })
  from: number;

  @Field(() => Int, { nullable: true })
  size: number;

  @Field(() => [Int], { nullable: true })
  categories?: number[];

  @Field({ nullable: true })
  searchTerm?: string;
}
