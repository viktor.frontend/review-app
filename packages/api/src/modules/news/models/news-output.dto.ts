import { Field, Int, ObjectType } from 'type-graphql';
import { News } from './news.entity';

@ObjectType()
export class NewsList {
  @Field(() => Int)
  count: number;

  @Field(() => [News])
  items: News[];
}
