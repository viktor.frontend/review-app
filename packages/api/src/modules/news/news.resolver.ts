import { NewsService } from './news.service';
import { News } from './models/news.entity';
import { Query, Resolver, Args } from '@nestjs/graphql';
import { UseGuards } from '@nestjs/common';
import { AuthGuard } from 'src/guards/auth.guard';
import { User } from 'src/decorators/user.decorator';
import { IKeycloakUser } from 'src/models/keycloak-user.model';
import { NewsArgs } from './models/news.dto';
import { NewsItemArgs } from './models/newsItem.dto';
import { NewsList } from './models/news-output.dto';

@UseGuards(AuthGuard)
@Resolver(() => News)
export class NewsResolver {
  constructor(private readonly newsService: NewsService) {}
  
  @Query(() => NewsList)
  news(
    @Args() args: NewsArgs,
    @User() user: IKeycloakUser
  ): Promise<NewsList> {
    return this.newsService.getAll({...args, roles: user.roles});
  }


  @Query(() => News)
  async newsItem(@Args() args: NewsItemArgs) {
    return this.newsService.getOneBySlug(args.slug);
  }

}
