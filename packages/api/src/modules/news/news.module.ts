import { Module } from '@nestjs/common';
import { NewsResolver } from './news.resolver';
import { NewsService } from './news.service';
import { News } from './models/news.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { NewsProvider } from './news.provider';
import { ElasticsearchModule, ElasticsearchModuleAsyncOptions } from '@nestjs/elasticsearch';
import { ConfigService } from '@nestjs/config';
import { NewsElasticService } from './news-elastic.service';
import { CategoryModule } from '../category/category.module';
import { RoleModule } from '../role/role.module';
import { TooltilesModule } from '../tooltiles/tooltile.module';

@Module({
  imports: [
    ElasticsearchModule.registerAsync({
      inject: [ConfigService],
      useFactory: (config: ConfigService) => config.get('elasticsearch'),
    } as ElasticsearchModuleAsyncOptions),
    TypeOrmModule.forFeature([News]),
    TooltilesModule,
    CategoryModule,
    RoleModule,
  ],
  providers: [NewsResolver, NewsService, NewsProvider, NewsElasticService],
})
export class NewsModule {}
