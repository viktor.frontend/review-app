import { Injectable } from '@nestjs/common';
import { AMQPController } from '../amqp/amqp.controller';
import { ConfigService } from '@nestjs/config';
import { NewsService } from './news.service';
import { NewsPayload } from './models/news.model';

const QUEUE = 'news';

@Injectable()
export class NewsProvider extends AMQPController<NewsPayload> {
  constructor(readonly configService: ConfigService, private readonly newsService: NewsService) {
    super(configService.get('amqp').connectionString, QUEUE, QUEUE);
  }

  async handleCreate(payload: NewsPayload) {
    await this.newsService.create(payload);
  }

  async handleUpdate(payload: NewsPayload) {
    await this.newsService.update(payload);
  }

  async handleDelete(payload: Partial<NewsPayload>) {
    await this.newsService.delete(payload);
  }
}
