import Vue from 'vue';
import VueRouter from 'vue-router';
import Dashboard from '../views/Dashboard.vue';
import NotFound from '../views/NotFound.vue';
import Faq from '../views/Faq.vue';
import News from '../views/News.vue';
import NewsItem from '../views/NewsItem.vue';
import SupportItem from '../views/SupportItem.vue';
import Contacts from '../views/Contacts.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'dashboard',
    component: Dashboard,
  },
  {
    path: '/faq',
    name: 'faq',
    component: Faq,
  },
  {
    path: '/contacts',
    name: 'contacts',
    component: Contacts,
  },
  {
    path: '/news',
    name: 'news',
    component: News,
  },
  {
    path: '/news/:slug',
    name: 'newsitem',
    component: NewsItem,
  },
  {
    path: '/support/:slug',
    name: 'supportitem',
    component: SupportItem,
  },
  {
    path: '*',
    name: 'notfound',
    component: NotFound,
  },
];

const router = new VueRouter({
  mode: 'history',
  routes,
});

export default router;
