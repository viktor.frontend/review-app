import { BootstrapVue, IconsPlugin } from 'bootstrap-vue';
import Vue from 'vue';
import VueApollo from 'vue-apollo';
import VueKeyCloak from '@dsb-norge/vue-keycloak-js';
import App from './App.vue';
import router from './router';
import { apolloProvider } from './graphql/main';

import './theme/main.scss';

Vue.config.productionTip = false;
Vue.use(BootstrapVue);
Vue.use(IconsPlugin);
Vue.use(VueApollo);

Vue.use(VueKeyCloak, {
  config: {
    realm: process.env.VUE_APP_KEYCLOAK_REALM,
    url: process.env.VUE_APP_KEYCLOAK_URL,
    clientId: process.env.VUE_APP_KEYCLOAK_CLIENTID,
  },
  init: { onLoad: 'login-required' },
  onReady: () => {
    new Vue({
      router,
      apolloProvider,
      render: h => h(App),
    }).$mount('#app');
  },
});
