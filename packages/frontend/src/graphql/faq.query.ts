import gql from 'graphql-tag';

export default gql`
  query faqs($categories: [Int!], $searchTerm: String) {
    faqs(categories: $categories, searchTerm: $searchTerm) {
      id
      title
      content
      showOnFaq
      categories {
        name
        color
      }
    }
  }
`;
