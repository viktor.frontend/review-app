import gql from 'graphql-tag';

export default gql`
  query supportPages {
    supportPages {
      id
      title
      slug
    }
  }
`;
