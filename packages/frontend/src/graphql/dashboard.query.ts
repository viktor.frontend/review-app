import gql from 'graphql-tag';

export default gql`
  query dashboard {
    dashboardCategories {
      id
      name
      order
      tooltiles {
        id
        title
        image
        url
        active
        categories {
          id
          name
          color
        }
      }
    }
  }
`;
