import Vue from 'vue';
import { ApolloClient } from 'apollo-client';
import { createHttpLink } from 'apollo-link-http';
import { InMemoryCache } from 'apollo-cache-inmemory';
import VueApollo from 'vue-apollo';
import { ApolloLink } from 'apollo-link';

const httpLink = createHttpLink({
  uri: process.env.VUE_APP_GRAPHQL_URL,
});
const middlewareLink = new ApolloLink((operation, forward) => {
  operation.setContext({
    headers: {
      authorization: `Bearer ${Vue.prototype.$keycloak.token}` || null,
    },
  });
  return forward(operation);
});

const cache = new InMemoryCache();

export const apolloClient = new ApolloClient({
  link: middlewareLink.concat(httpLink),
  cache,
});

export const apolloProvider = new VueApollo({
  defaultClient: apolloClient,
});
