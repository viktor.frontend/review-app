import gql from 'graphql-tag';

export default gql`
  query newsItem($slug: String!) {
    newsItem(slug: $slug) {
      title
      content
      image
      updated
      slug
      relatedNews {
        title
        slug
      }
      categories {
        id
        name
        color
      }
      tooltiles {
        id
        title
        image
        url
        active
        categories {
          id
          name
          color
        }
      }
    }
  }
`;
