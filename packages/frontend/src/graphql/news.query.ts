import gql from 'graphql-tag';

export default gql`
  query news($from: Int!, $categories: [Int!], $searchTerm: String) {
    news(from: $from, categories: $categories, searchTerm: $searchTerm) {
      count
      items {
        id
        title
        shortContent
        image
        updated
        slug
        showOnTop
      }
    }
  }
`;
