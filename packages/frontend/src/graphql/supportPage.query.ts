import gql from 'graphql-tag';

export default gql`
  query supportPage($slug: String!) {
    supportPage(slug: $slug) {
      title
      content
      image
      updated
      slug
      relatedSupportPages {
        title
        slug
      }
      tooltiles {
        id
        title
        image
        url
        active
        categories {
          id
          name
          color
        }
      }
    }
  }
`;
